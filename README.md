# snake-backend

## Setup
First, clone the repository.

`git clone git@gitlab.com:skn_MOS/snake/snake-backend.git`

It's advised to create a virtual environment using your preferred method.
The most basic command to do that would be

`python -m venv [path to venv folder]`

After installing and activating the virtual environment, install the
 dependencies using

`pip install -r requirements.txt`

## Running the application locally
To get the application running locally, run the following command in 
the root folder of the project:

`uvicorn app.main:app --reload`

## Running the test suite
To run the whole test suite, run the following command in 
the root folder of the project:

`pytest`
